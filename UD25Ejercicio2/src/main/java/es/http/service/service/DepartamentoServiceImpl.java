package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.IDepartamentoDAO;
import es.http.service.dto.Departamento;

@Service
public class DepartamentoServiceImpl {
	
	@Autowired
	IDepartamentoDAO iDepartamentoDAO;
	
	public List<Departamento> listarDepartamento() {
		return iDepartamentoDAO.findAll();
	}
	
	public Departamento guardarDepartamento(Departamento departamento) {
		return iDepartamentoDAO.save(departamento);
	}
	
	public Departamento encontrarDepartamentoId(Long id) {
		return iDepartamentoDAO.findById(id).get();
	}
	
	public Departamento actualizarDepartamento(Departamento departamento) {
		return iDepartamentoDAO.save(departamento);
	}
	
	public void eliminarDepartamento(Long id) {
		iDepartamentoDAO.deleteById(id);
	}
}
