package es.http.service.service;

import java.util.List;

import es.http.service.dto.Empleado;

public interface IEmpleadoService {

	public List<Empleado> listarEmpleados();
	
	public Empleado guardarEmpleado(Empleado empleado);

	public Empleado encontrarEmpleadoId(String id);
	
	public Empleado actualizarEmpleado(Empleado empleado);
	
	public void eliminarEmpleado(String id);
}
