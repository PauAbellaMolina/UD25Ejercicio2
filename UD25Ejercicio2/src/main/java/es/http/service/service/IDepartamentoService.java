package es.http.service.service;

import java.util.List;

import es.http.service.dto.Departamento;

public interface IDepartamentoService {

public List<Departamento> listarDepartamentos();
	
	public Departamento guardarDepartamento(Departamento departamento);

	public Departamento encontrarDepartamentoId(Long id);
	
	public Departamento actualizarDepartamento(Departamento departamento);
	
	public void eliminarDepartamento(Long id);
}
