package es.http.service.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import es.http.service.dto.Departamento;

public interface IDepartamentoDAO extends JpaRepository<Departamento, Long> {

}
