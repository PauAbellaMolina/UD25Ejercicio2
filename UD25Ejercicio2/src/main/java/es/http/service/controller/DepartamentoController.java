package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Departamento;
import es.http.service.service.DepartamentoServiceImpl;

@RestController
@RequestMapping("/api")
public class DepartamentoController {
	
	@Autowired
	DepartamentoServiceImpl departamentoServiceImpl;
	
	@GetMapping("/departamentos")
	public List<Departamento> listarDepartamento() {
		return departamentoServiceImpl.listarDepartamento();
	}
	
	@PostMapping("/departamentos")
	public Departamento guardarDepartamentoController(@RequestBody Departamento departamento){
		return departamentoServiceImpl.guardarDepartamento(departamento);
	}
	
	@GetMapping("/departamentos/{id}")
	public Departamento encontrarDepartamentoIdController(@PathVariable(name="id") Long id){
		Departamento departamentoEncontrado = new Departamento();
		departamentoEncontrado = departamentoServiceImpl.encontrarDepartamentoId(id);
		return departamentoEncontrado;
	}
	
	@PutMapping("/departamentos/{id}")
	public Departamento actualizarDepartamentoController(@PathVariable(name="id") Long id, @RequestBody Departamento departamento){
		Departamento departamentoSeleccionado = new Departamento();
		Departamento departamentoActualizado = new Departamento();
		
		departamentoSeleccionado = departamentoServiceImpl.encontrarDepartamentoId(id);
		
		departamentoSeleccionado.setNombre(departamento.getNombre());
		
		departamentoActualizado = departamentoServiceImpl.actualizarDepartamento(departamentoSeleccionado);
		
		return departamentoActualizado;
	}
	
	@DeleteMapping("/departamentos/{id}")
	public void eliminarDepartamentoController(@PathVariable(name="id") Long id){
		departamentoServiceImpl.eliminarDepartamento(id);
	}
}
