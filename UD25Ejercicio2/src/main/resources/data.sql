DROP table IF EXISTS empleados;
DROP table IF EXISTS departamentos;

CREATE TABLE `departamentos` (
  `id` long NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `presupuesto` long NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `empleados` (
  `id` varchar(8) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `dep_id` long DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `departamentos_fk` FOREIGN KEY (`dep_id`) REFERENCES `departamentos` (`id`)
);

insert into departamentos (nombre,presupuesto)values('Informatica',1000);
insert into departamentos (nombre,presupuesto)values('Marketing',2000);
insert into departamentos (nombre,presupuesto)values('Administracion',3000);

insert into empleados (id,nombre,apellidos,dep_id)values('11111111','Pau','Abella',1);
insert into empleados (id,nombre,apellidos,dep_id)values('22222222','Josep','Montero',2);
insert into empleados (id,nombre,apellidos,dep_id)values('33333333','Jhon','Brown',3);
